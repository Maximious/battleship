//stop illegal entrance
let player1Data = JSON.parse(localStorage.getItem('player1'));
let player2Data = JSON.parse(localStorage.getItem('player2'));
let turn = localStorage.getItem('turn');

if (player1Data == null && player2Data == null)
{
    window.location.assign("battleship-welcome.html"); 
}

//audio effects
let missAudio = new Audio('battleship-assets/miss.mp3');
let hitAudio = new Audio('battleship-assets/hit.mp3');
let winAudio = new Audio('battleship-assets/win.mp3');

//return non attacking player
function getCurPlayer()
{
    return turn == 1 ? player2Data : player1Data;
}

function returnCurPlayer(playerData)
{
    if (turn == 1)
        player2Data = playerData;
    else
        player1Data = playerData;
}

//coloring the boards
function placeShips(playerData, table)
{
    if(turn == table+1)
    {
        Object.keys(playerData["boats"]).forEach(el => {

            $("#" + table + el).css("background-color", "yellow");
            //document.getElementById(""+table+el).setAttribute("style", "background-color: yellow;");
            
        });
    }
    else
    {
        Object.keys(playerData["boats"]).forEach(el => {
            //if (document.getElementById(""+table+el).firstChild == null)
            if (playerData["boats"][el] == 1)
                $("#" + table + el).css("background-color", "grey");
                //document.getElementById(""+table+el).setAttribute("style", "background-color: grey;");     
    });
    }
}

$(document).ready(function() {
    
    $("#player1").text(player1Data["player"]);
    $("#player2").text(player2Data["player"]);

    placeShips(player1Data, 0);
    placeShips(player2Data, 1);

    player1Data["remaining"] = 10;
    player2Data["remaining"] = 10;

    localStorage.removeItem("player1");
    localStorage.removeItem("player2");
    localStorage.removeItem("turn");

    $(".field").each(function (index, element) {
        element.addEventListener('click', function(e) 
        {
            e.preventDefault();
            setX(e.target);
        }); 
    });
    
});


//checks if the whole ship is sunk
function shipSunk(idField)
{
    let curPlayer = getCurPlayer();
    let x = parseInt(idField[1]); 
    let y = parseInt(idField[2]);
    //left, down, right and up
    let xPath = [0,1,0,-1];
    let yPath = [-1,0,1,0];

    for(let i = 0; i < xPath.length; i++)
    {
        let xCur = x; 
        let yCur = y;
        let flag = true;

        while(true)
        {
            xCur+=xPath[i];
            yCur+=yPath[i];

            //another ship is placed near
            if (curPlayer["boats"][xCur+""+yCur] == 1)
            {
                //console.log("Ship there - player: " + curPlayer["player"]);
                flag = false;
                break;
            }

            //the field is empty
            if (typeof curPlayer["boats"][xCur+""+yCur] === 'undefined')
            {
                //console.log("Nothing there!");
                break;
            }
        }

        if(!flag)
           return false; 
    }

    return true;
}

//on click
function setX(el)
{
    let opponent = getCurPlayer();
    console.log("Player: " + opponent["player"] + "  remaining: " +  opponent["remaining"]);
    if(el.lastChild != null || el.id[0] != turn%2)
    {
        return;
    }

    let xImg = document.createElement("img");
    let fieldId = el.id.slice(1,);

    if (opponent["boats"][fieldId] == 1)
    {
        xImg.src = "battleship-assets/x.png";
        hitAudio.play();
        $("#" + el.id).css("background-color", "yellow");
        //el.setAttribute("style", "background-color: yellow;");
        opponent["boats"][fieldId]=0;
        
        if (shipSunk(el.id))
        {
            alert("You have sunk a ship!");
            opponent["remaining"]--;
        }
    }
    else
    {
        xImg.src = "battleship-assets/o.png";
        missAudio.play();
        returnCurPlayer(opponent);
        turn=1+turn%2;
        placeShips(player1Data, 0);
        placeShips(player2Data, 1);
    }
    xImg.setAttribute("draggable", "false");
    el.appendChild(xImg);

    if(player1Data["remaining"] == 0)
    {
        winAudio.play();
        alert(player1Data["player"] + " has won the game!!!\nRezultat: " + player2Data["remaining"]);
        window.location.assign("battleship-welcome.html"); 
        return;
    }
    if(player2Data["remaining"] == 0)
    {
        winAudio.play();
        alert(player1Data["player"] + " has won the game!!!\nRezultat: " + player1Data["remaining"]);
        window.location.assign("battleship-welcome.html"); 
        return;
    }        
}