//stop illegal entrance
let player1Data = JSON.parse(localStorage.getItem('player1'));
let player2Data = JSON.parse(localStorage.getItem('player2'));

if (player1Data == null && player2Data == null)
{
    window.location.assign("battleship-welcome.html"); 
}

//coordinates where new ship should be placed
let xField = "";
let xTimeout;
let yField = "";

//color tiles for requested player
function placeShips(playerData)
{
    //color ships yellow
    Object.keys(playerData["boats"]).forEach(el => {
        $("#"+el).css("background-color", "yellow");
    });

    //update counters
    $("#1BoatCounter").text(playerData["counters"][0]);
    $("#2BoatCounter").text(playerData["counters"][1]);
    $("#3BoatCounter").text(playerData["counters"][2]);
    $("#4BoatCounter").text(playerData["counters"][3]);
}

function getTurn(){ return parseInt(localStorage.getItem("turn"));  }

function getCurPlayer()
{
    let turn = getTurn();
    return turn == 1 ? player1Data : player2Data;
}

function setCurPlayer(playerData)
{
    let turn = getTurn();
    if(turn == 1)
        player1Data = playerData;
    else
        player2Data = playerData;
}

$(document).ready(function() {

    let playerData = getCurPlayer();
    $("#player").text(playerData["player"] + ' postavlja brodove:');
    placeShips(playerData);

    //adding remove function on trggered on right click
    $(".field").each(function (index, element) {
        element.addEventListener('contextmenu', function(e) 
        {
            //console.log("Deleting!")
            if(e.button == 2) {
              e.preventDefault();
              deleteShip(e.target);
            }
        }); 
    });
    //memorizing the field from which dragging started on left click
    $(".field").each(function (index, element) {
        element.addEventListener('mousedown', function(e) 
        {
            //console.log("Pressing!");
            if(e.button == 0) {
              e.preventDefault();
              startDragging(e.target);
            }
        }); 
    });
    $(".container").each(function (index, element) {
        element.addEventListener('mouseup', function(e) 
        {
            console.log("Target: " + e.target.tagName);
            if($(e.target).attr('class') == 'field')
            {
                //adding an event on left click released
                if(e.button == 0) {
                    e.preventDefault();
                    stopDragging(e.target);
                  }
            }
            else
            {
                if (xField != "")
                {
                    //remove red color from starting point after some time
                    console.log("BackToAlmostBlackCalled");
                    if (getCurPlayer()["boats"][xField] == 1)
                        $("#"+xField).css("background-color","yellow");
                    else
                        $("#"+xField).css("background-color","grey");
                    xField="";
                }
            }
        }); 
    });
       
});

//are all the ships placed on the board
function checkCounters()
{  
    let oneCounter = parseInt(document.getElementById('1BoatCounter').textContent);
    let twoCounter = parseInt(document.getElementById("2BoatCounter").textContent);
    let threeCounter = parseInt(document.getElementById("3BoatCounter").textContent);
    let fourCounter = parseInt(document.getElementById("4BoatCounter").textContent);
    if (oneCounter == 0 && twoCounter == 0 && threeCounter == 0 && fourCounter == 0)
    {
        return true;
    }
    return false;
}

//helper functions which recursively delets all parts of the ship
function recDelete(player, pos)
{
    $("#"+pos).css("background-color","grey");
    //console.log(pos);
    delete player["boats"][pos];

    let x = parseInt(pos[0]);
    let y = parseInt(pos[1]);

    let num = 0;

    if (player["boats"][""+x+(y-1)])
    {//left
        num+=recDelete(player,""+x+(y-1));
    }
    if (player["boats"][""+x+(y+1)])
    {//right
        num+=recDelete(player,""+x+(y+1));
    }
    if (player["boats"][""+(x+1)+y])
    {//down
        num+=recDelete(player,""+(x+1)+y);
    }
    if (player["boats"][""+(x-1)+y])
    {//up
        num+=recDelete(player,""+(x-1)+y);
    }

    //returning the length of ship deleted
    return num+1;
}

function deleteShip(el)
{
    let playerData=getCurPlayer();

    if(playerData["boats"][el.id] != 1)
        return;

    //returns the length of a ship it removed
    switch(recDelete(playerData,el.id))
    {
        case 1:
            $("#1BoatCounter").text((parseInt($("#1BoatCounter").text())+1));
            //document.getElementById("1BoatCounter").textContent = (parseInt(document.getElementById("1BoatCounter").textContent)+1);
            playerData["counters"][0]++;
            break;
        case 2:
            $("#2BoatCounter").text((parseInt($("#2BoatCounter").text())+1));
            //document.getElementById("2BoatCounter").textContent = (parseInt(document.getElementById("2BoatCounter").textContent)+1);
            playerData["counters"][1]++;
            break;
        case 3:
            $("#3BoatCounter").text((parseInt($("#3BoatCounter").text())+1));
            //document.getElementById("3BoatCounter").textContent = (parseInt(document.getElementById("3BoatCounter").textContent)+1);
            playerData["counters"][2]++;
            break;
        case 4:
            $("#4BoatCounter").text((parseInt($("#4BoatCounter").text())+1));
            //document.getElementById("4BoatCounter").textContent = (parseInt(document.getElementById("4BoatCounter").textContent)+1);
            playerData["counters"][3]++;
            break;
    }
}

//checks whether the ship can be placed at desired position
function checkPosition(start, end, horizontal, staticField)
{
    /*
    x - nope s- ship(also nope)

    xxxx    xxx
    xssx    xsx
    xxxx    xsx 
            xxx
    */
    let playerData = getCurPlayer();
    let static = parseInt(staticField);
    
    if (horizontal)
    {
        for(let i = static-1; i <= static+1; i++)
            for(let j = start-1; j <= end+1; j++)
            {
                if(playerData["boats"][""+i+j] == 1)
                {
                    //alert("I: "+i+"  J: "+j);
                    return false;
                }
            }
    }
    else
    {
        for(let i = start-1; i <= end+1; i++)
            for(let j = static-1; j <= static+1; j++)
            {
                if(playerData["boats"][""+i+j] == 1)
                {
                    //alert("I: "+i+"  J: "+j);
                    return false;
                }
            }
    }   
    return true;
}

//memorize the start point
function startDragging(el)
{
    xField = el.id;
    yField = "";
    el.setAttribute("style", "background-color: red;");
    console.log("xField set to: " + xField);
}

//place a ship if possible
function colorTiles()
{
    //direction of the boat
    let horizontal = xField[0] == yField[0];
    let curCounter = 0;
    let length = horizontal ? Math.abs(xField[1] - yField[1]) + 1 : Math.abs(xField[0] - yField[0]) + 1; 

    if (length > 4)
    {
        alert("You can't place a ship that long!\nTry again");
        return;
    }

    switch(length)
    {
        case 1:
            curCounter = parseInt($("#1BoatCounter").text());
            //curCounter = parseInt(document.getElementById("1BoatCounter").textContent);
            break;
        case 2:
            curCounter = parseInt($("#2BoatCounter").text());
            //curCounter = parseInt(document.getElementById(2BoatCounter").textContent);
            break;
        case 3:
            curCounter = parseInt($("#3BoatCounter").text());
            //curCounter = parseInt(document.getElementById("3BoatCounter").textContent);
            break;
        case 4:
            curCounter = parseInt($("#4BoatCounter").text());
            //curCounter = parseInt(document.getElementById("4BoatCounter").textContent);
            break;
    }
    
    if (curCounter == 0)
    {
        alert("You don't have any ships left this long!");
        return;
    }
    else
    {
        curCounter--;
    }

    if (horizontal)
    {
        let start = parseInt(xField[1] < yField[1] ? xField[1] : yField[1]);
        let end = parseInt(xField[1] > yField[1] ? xField[1] : yField[1]);

        if (!checkPosition(start, end, horizontal,xField[0]))
        {
            alert("You can't place two boats near each other!!!");
            return;
        }

        let playerData = getCurPlayer();

        for (let i = start; i <= end; i++)
        {
            $("#"+xField[0].concat(i)).css("background-color", "yellow");
            //document.getElementById(xField[0].concat(i)).setAttribute("style", "background-color: yellow;");
            
            playerData["boats"][xField[0].concat(i)] = 1;
        }

        setCurPlayer(playerData);
    }
    else
    {
        let start = parseInt(xField[0] < yField[0] ? xField[0] : yField[0]);
        let end = parseInt(xField[0] > yField[0] ? xField[0] : yField[0]);

        if (!checkPosition(start, end, horizontal,xField[1]))
        {
            alert("You can't place two boats near each other!!!");
            return;
        }

        let playerData = getCurPlayer();

        for (let i = start; i <= end; i++)
        {
            $("#" + i + xField[1]).css("background-color", "yellow");
            //document.getElementById(i + xField[1]).setAttribute("style", "background-color: yellow;");
            
            playerData["boats"][i + xField[1]] = 1;
        }

        setCurPlayer(playerData);
    }

    switch(length)
    {
        case 1:
            $("#1BoatCounter").text(curCounter);
            //document.getElementById("1BoatCounter").textContent = curCounter;
            break;
        case 2:
            $("#2BoatCounter").text(curCounter);
            //document.getElementById("2BoatCounter").textContent = curCounter;
            break;
        case 3:
            $("#3BoatCounter").text(curCounter);
            //document.getElementById("3BoatCounter").textContent = curCounter;
            break;
        case 4:
            $("#4BoatCounter").text(curCounter);
            //document.getElementById("4BoatCounter").textContent = curCounter;
            break;
    }

    let turn = getTurn();
    //console.log(length);

    if(turn == 1)
    {
        player1Data["counters"][length-1]--;
        //console.log(player1Data["counters"][length-1]);
    }
    else
    {
        player2Data["counters"][length-1]--;
        //console.log(player2Data["counters"][length-1]);
    }
}

//place a ship on dragged fields
function stopDragging(el)
{
    console.log("stopDragging called");
    if(xField == "")
    {
        console.log("xField is empty");
        return;
    }
    clearTimeout(xTimeout);
    yField = el.id;
    let playerData = getCurPlayer();

    //there is already a ship at xField position?
    if( playerData["boats"][xField] == 1)
        $("#"+xField).css("background-color", "yellow");
       // document.getElementById(xField).setAttribute("style", "background-color: yellow;");
    else
        $("#"+xField).css("background-color", "grey");
        //document.getElementById(xField).setAttribute("style", "background-color: grey;");

    if (xField[0] != yField[0] && xField[1] != yField[1])
    {
        alert("You can't place ships in a diagonal way!!!\n Try again");
        return;
    }

    //try to place a ship
    colorTiles();

}

function back()
{
    let turn = getTurn();

    if (turn == 1)
    {
        //return to the welcome page
        window.location.href = "battleship-welcome.html";        
    }
    else
    {
        //return to setup page for player 1
        localStorage.setItem("player2", JSON.stringify(player2Data));
        localStorage.setItem("turn","1");   
        window.location.href = "battleship-setup.html";        
    }  
}

function next()
{
    let turn = getTurn();

    if (!checkCounters())
    {
        alert("You didn't post all the ships on the field!");
        return;
    }

    if (turn == 1)
    {
         //go to setup page for player 2
        localStorage.setItem("player1", JSON.stringify(player1Data));
        window.location.href = "battleship-setup.html";      
        localStorage.setItem("turn","2");   
    }
    else
    {
        //go to the game
        localStorage.setItem("player2", JSON.stringify(player2Data));
        localStorage.setItem("turn","1");   
        window.location.href = "battleship-game.html";        
    }  
}
