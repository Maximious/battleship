$(document).ready(function () 
{
    //if someone pressed back from setup page
    let player1Data = JSON.parse(localStorage.getItem('player1'));
    let player2Data = JSON.parse(localStorage.getItem('player2'));

    if (player1Data != null)
    {
        $("#player1").val(player1Data["player"]);
        $("#player2").val(player2Data["player"]);
    }

    $("#loginForm").bind("submit", function (e) {

        //remove standard action ?
        e.preventDefault(); 

        //username can use letters (upper or lower), numbers and underscore
        let regex= /(\d|\w)+/g; 
        let player1 = document.getElementById("player1").value;
        let player2 = document.getElementById("player2").value;
        
        //checking min and max length
        if (player1.length > 15 || player1.length < 3)
        {
            alert("Username for player1 is not acceptable!\nUsername must have between 3 and 15 characters." + "\nTry again!");
            return;
        }

        let foundP1 = player1.match(regex);

        //stop usernames with forbidden characters (!@#sth - passes first condition but not second)
        if(foundP1.length != 1 || foundP1[0].length != player1.length)
        {
            alert("Username for player1 is not acceptable!\nYou can only use letters, numbers and underscore." + "\nTry again!");
            return;
        }

        if (player2.length > 15 || player2.length < 3)
        {
            alert("Username for player2 is not acceptable!\nUsername must have between 3 and 15 characters." + "\nTry again!");
            return;
        }

        let foundP2 = player2.match(regex);

        if(foundP2.length != 1 || foundP2[0].length != player2.length)
        {
            alert("Username for player2 is not acceptable!\nYou can only use letters, numbers and underscore." + "\nTry again!");
            return;
        }

        //structures for further use
        let player1Data = '{ "player" : "' + player1 + '", "boats" : {}, "counters" : [4,3,2,1]}';
        let player2Data = '{ "player" : "' + player2 + '", "boats" : {}, "counters" : [4,3,2,1]}';
        localStorage.setItem('player1', player1Data);
        localStorage.setItem('player2', player2Data);
        localStorage.setItem('turn', "1");

        //placing the ships
        window.location.href = "battleship-setup.html"; 
    });

});

